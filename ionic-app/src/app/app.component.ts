import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ServiceService } from './api/service.service';
import { Post } from './api/api';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent{
  // list: Post[] = [];
  constructor(
    public postService: ServiceService,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}

export class PostComponent implements OnInit {
  list : Post[] = [];
 
  // Si usa per definire iniezioni dei servizi e cose immutabili della classe
  constructor(
    public postService: ServiceService
  ) { }
  
  // Viene eseguito all'inizializzazione DOPO il costruttore e si usa per caricare i dati 
  ngOnInit() { 
   // this.getPosts(5);
  }

  // getPosts(elements?: number) : void {
  //   this.postService.get().then(
  //     (a) => {
  //       this.list = a.slice(0, elements + this.list.length);
  //     },
  //     (b) => console.log("Err", b)
  //   );
 //}

  
}

export class PostDetailComponent implements OnInit {
  post: Post | boolean;
  
  constructor(
    private route:ActivatedRoute, 
    private location: Location,
    public postService: ServiceService
  ) { }
  
  ngOnInit() { 
    this.getPostById(parseInt( this.route.snapshot.paramMap.get('id')));
  }

  getPostById(id: number) {
    this.postService.getById(id).then(
      (data) => {
        if (!data) 
          this.post = false;
        else 
          this.post = data;
      },
      (error) => console.log("Err", error)
    );
  }
  
}
