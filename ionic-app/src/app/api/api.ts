export class Post extends Object {
    userId?: number;
    id: number;
    title: string;
    body: string;
  }
