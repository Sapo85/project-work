import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post } from './api';


@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(
    private http: HttpClient
    )
 { }

  get():Promise<Post[]> {

    return this.http.get<Post[]>("https://riminidavivere.immaginificio-test.com/wp-json/wp/v2/posts").toPromise();
  }

  getById(id: number):Promise<Post> {

    return this.http.get<Post>("https://riminidavivere.immaginificio-test.com/wp-json/wp/v2/posts"+id).toPromise();
  }
  

}
