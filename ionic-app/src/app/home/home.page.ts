import { Component, OnInit } from '@angular/core';
import { Post } from '../api/api';
import { ServiceService } from '../api/service.service';
import { Platform, NavController } from '@ionic/angular';
import { SinglePagePage } from '../single-page/single-page.page';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  list: Post[] = [];
  constructor(
    public postService: ServiceService,
    private platform: Platform,
    public navCtrl: NavController

  ) {
    this.getPosts(5);
  }

  ngOnInit(): void { 
    //this.getPosts(5);
  }

  getPosts(elements?: number) : void {
    this.postService.get().then(
      (a) => {
        this.list = a.slice(0, elements + this.list.length);
        console.log('homePage - getPosts() - this.list',this.list)
      },
      (b) => console.log("Err", b)
    );
  }

  onEventoDettaglio(post:Post){
    this.navCtrl.navigateRoot('/single-page/(post:post)');
    this.navCtrl.
  }

}
